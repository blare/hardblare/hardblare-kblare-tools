/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 * Copyright (C) 2018 Mounir Nasr Allah <mounir.nasrallah@centralesupelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 *  Mounir Nasr Allah <mounir.nasrallah@centralesupelec.fr>
 */

#include "lib.h"
#include <sys/types.h>
#include <attr/xattr.h> // depends on package libattr1-dev
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <errno.h>
#include <error.h>
#include <string.h>

#define TAGS_PER_INT (8*sizeof(int))

/* Used by qsort as a comparation function */
static inline int compare (const void *a, const void *b){
	const int *ia = (const int*)a;
	const int *ib = (const int*)b;
	return (*ia > *ib) - (*ia < *ib);
}

/* Get the number extended attributes for file @filename.
 * Returns the number of attributes or the error code.
 * */
int get_list_length(char *filename){
	int rc;

	rc = listxattr(filename, NULL,0);

	return rc;
}

/* Get the list of extended attributes for file @filename.
 * Returns the number of attributes or the error code.
 * */
int get_list(char *filename, char *list, int n){
	int rc;

	rc = listxattr(filename,list,n);
	if (rc<0){
		return rc;
	}

	return rc;
}

/* Allocate and free the memory needed to store tags
 * */
void init_tags(struct tags_t *tags, int n) {
	tags->size = n;
	if (n > 0)
		tags->values = (int*) malloc(n * sizeof(int));
}

void free_tags(struct tags_t *tags) {
	tags->size = 0;
	free(tags->values);
}

/* get_tags_* functions allocate the memory needed to store the extended
 * attributes, then fetch and store these attributes.
 * The memory should be freed with free_tags afterwards
 * */
int get_tags_from_file(char *filename, char *xattr_name, struct tags_t *tags) {
	int n, rc;
	int i,j;

	n = getxattr(filename, xattr_name, NULL , 0);
	if (n<0 && errno != ERANGE) {
		perror("Could not fetch tags size");
		return n;
	}

	if (n == 0) {
		init_tags(tags, 0);
		return 0;
	}

	int *tags_bitfield = malloc(n);
	rc = getxattr(filename, xattr_name, tags_bitfield, n);
	if (rc<0) {
		perror("Could not fetch tags");
		return rc;
	}
	int tags_nb_int = n/sizeof(int);
	int count = 0;
	for (i=0 ; i<tags_nb_int ; i++)
		count += __builtin_popcount(tags_bitfield[i]);
	fprintf(stderr,"%i tags detected\n",count);
	init_tags(tags, count);
	int k=0;
	for (i=0 ; i<tags_nb_int ; i++)
		for (j=0 ; j<TAGS_PER_INT ; j++)
			if (tags_bitfield[i] & (1 << j))
				tags->values[k++] = i * TAGS_PER_INT + j;

	free(tags_bitfield);
	return 0;
}

int get_tags_from_command(int argc, char *argv[], struct tags_t *tags) {
	int n, i;

	n = argc - 2;
	if (n<1)
		return -1;

	/* The number of tags in argv[] is argc -2 (the first two values are
	 * the program name and the filename). */
	init_tags(tags, n);

	for(i=0; i<n; i++)
		tags->values[i] = atoi(argv[i+2]);

	// Now let's sort it
	qsort(tags->values,n,sizeof(int),compare);

	// Return the number of tags
	return n;
}

int write_tags(char *filename, char *xattr_name, struct tags_t tags) {
	int rc;
	int i;

	int values[BLARE_TAGS_NUMBER] = {0};
	for (i=0 ; i<tags.size ; i++) {
		if (tags.values[i] < 0 || tags.values[i] >= BLARE_TAGS_NUMBER * TAGS_PER_INT) {
			fprintf(stderr, "Invalid tag (off-range): %i\n", tags.values[i]);
			continue;
		}
		int index = tags.values[i] / TAGS_PER_INT;
		int offset = tags.values[i] % TAGS_PER_INT;
		fprintf(stderr,"Bit goes at position %i,%i\n",index,offset);
		values[index] |= (1 << offset);
	}

	rc = setxattr(filename, xattr_name, values, sizeof(values), 0x0);
	/* 0x0 = XATTR_CREATE|XATTR_REPLACE); */
	if (rc < 0) {
		perror("Could not write tags");
	}

	return rc;
}
