/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	int rc, count;
	char *filename;
	struct tags_t itags;

	count = argc - 2;
	if (count < 0) {
		printf("Usage: setinfo fichier tag1 [tag2 ...]\n");
		return -1;
	}
	if (count == 0){
		printf("Nothing to do.\n");
		return 0;
	}
	count = get_tags_from_command(argc,argv,&itags);
	if (count < 0) {
		printf("Could not parse tags properly.\n");
		return count;
	}

	filename = argv[1];

	rc = write_tags(filename, INFO_XATTR, itags);
	free_tags(&itags);
	if (rc < 0) {
		printf("Could not write tags: error %d\n",rc);
		return rc;
	}

	printf("Successfuly wrote tags\n");
	return 0;
}
