#!/bin/sh

for i in `find $1 -print` ; do
    getinfo $i
    if [ $? -eq 0 ] ; then
    delinfo $i
    echo -n " --> deleted from $i\n"
    fi
done
