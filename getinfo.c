/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include "lib.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	int rc, i;
	char *filename;
	struct tags_t itags;

	if (argc < 2){
		printf("Usage: getinfo filename\n");
		return -1;
	}

	filename = argv[1];
	rc = get_tags_from_file(filename, INFO_XATTR, &itags);
	if (rc<0) {
		printf("Could not read information tags.\n");
		return rc;
	}

	printf("itags: ");
	for(i=0 ; i<itags.size ; i++)
		printf("%d ",itags.values[i]);

	printf("\n");

	free_tags(&itags);
	return 0;
}
