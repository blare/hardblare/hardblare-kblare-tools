/*
 * Copyright (C) 2010 Christophe Hauser <christophe.hauser@supelec.fr>
 * Copyright (C) 2013 Guillaume Brogi <guillaume.brogi@supelec.fr>
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation, version 2.
 *
 * Author:
 * 	Christophe Hauser <christophe.hauser@supelec.fr>
 * 	Guillaume Brogi <guillaume.brogi@supelec.fr>
 */

#include <stdbool.h>

#define DEBUG false

#define INFO_XATTR "security.blare.tag"
#define BLARE_TAGS_NUMBER 4

struct tags_t {
	int size;
	int* values;
};

int get_list_length(char *filename);
int get_list(char *filename, char *list, int n);

int get_tags_from_file(char *filename, char *xattr_name, struct tags_t *tags);
int get_tags_from_command(int argc, char *argv[], struct tags_t *tags);
int write_tags(char *filename, char *xattr_name, struct tags_t tags);
void free_tags(struct tags_t *tags);
