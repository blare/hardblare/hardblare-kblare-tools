TARGETS = setinfo getinfo list_xattr delinfo
RM = /bin/rm -f
CP = /bin/cp
DESTDIR = /usr/local/bin

all: $(TARGETS)

clean:
	$(RM) *.o $(TARGETS)

install:
	$(CP) $(TARGETS) $(DESTDIR)

setinfo : lib.o setinfo.o
delinfo : lib.o delinfo.o
getinfo : lib.o getinfo.o
list_xattr : lib.o list_xattr.o
